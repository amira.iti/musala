################## Running Commands ##################

# robot -d Results      -i     Happy        Tests
# robot -d Results      -i     Negative     Tests
# robot -d Results      -i     TC4     Tests
# robot -d Results      -i     musala        Tests


*** Settings ***
Library           SeleniumLibrary
Resource         ../Resources/common.robot
Resource         ../Resources/PO/TC4.robot
Test Setup       Begin web test
Test Teardown    End web test

*** Variables ***
${open_position_url}           https://www.musala.com/careers/join-us/
${sofia_positions}             https://www.musala.com/careers/join-us/?location=Sofia
${Skopje_positions}            https://www.musala.com/careers/join-us/?location=Skopje

*** Test Cases ***

Get all open positions in Sofia city
     [Tags]     open_positions    Happy  TC4     TCs     musala

     Access career Link
     Access Open Positions
     ${current_url}     Get Current URL
     Assert current url      ${current_url}    ${open_position_url}
     Select Location    Sofia
     ${current_url}     Get Current URL
     Assert current url      ${current_url}    ${sofia_positions}
     Log to console     All Open Positions in Sofia ${\n}
     Log      All Open Positions in Sofia ${\n}

     Get All Open position in a City

Get all open positions in Skopje city
     [Tags]     open_positions    Happy  TC4     TCs
     Access career Link
     Access Open Positions
     ${current_url}     Get Current URL
     Assert current url      ${current_url}    ${open_position_url}
     Select Location    Skopje
     ${current_url}     Get Current URL
     Assert current url      ${current_url}    ${Skopje_positions}
     Log      All Open Positions in Skopje ${\n}
     Get All Open position in a City








