*** Settings ***
Library    SeleniumLibrary
Resource  ../common.robot

*** Variables ***
${career}                  xpath=//div[@id='menu']/ul[@class='nav']//a[@href='https://www.musala.com/careers/']
${open_postion}            xpath=//span[@data-alt='Check our open positions']
${view_postion_details}    xpath=(//img[contains(@alt,'${name_of_postion}')])[1]
#${job_sections}            xpath=//h2[contains(.,'${section_name}')]
${general_description}     xpath=//h2[contains(.,'General description')]
${requirements}            xpath=//h2[contains(.,'Requirements')]
${responsibilities}        xpath=//h2[contains(.,'Responsibilities')]
${What_we_offer}           xpath=//h2[contains(.,'What we offer')]
${apply_btn}                xpath=//input[@type='button']
${name}                    xpath=//input[@id='cf-1']
${email}                   xpath=//input[@id='cf-2']
${mobile}                  xpath=//input[@id='cf-3']
${upload_file}             xpath=//input[@id='uploadtextfield']
${file_path}               ${EXECDIR}/Resources/CV/QA_Lead.pdf

*** Keywords ***

Access Career Link
    Wait Until Element is visible    ${career}
    Click Element    ${career}


Choose open position by name
        [Arguments]     ${name_of_postion}
        Wait Until Element is visible    ${view_postion_details}
        Click Element    ${view_postion_details}

Assert Job Sections
       Wait Until Element is visible    ${general_description}
       Wait Until Element is visible    ${requirements}
       Wait Until Element is visible    ${responsibilities}
       Wait Until Element is visible    ${What_we_offer}

Click To Apply
       Wait Until Element is visible    ${apply_btn}
       Click Element    ${apply_btn}

Fill Applicant Data
    [Arguments]     ${a_name}      ${a_email}        ${a_mobile}
      input text     ${name}       ${a_name}
      input text     ${email}      ${a_email}
      input text     ${mobile}     ${a_mobile}
Upload CV
#   Click Element    ${upload_file}
   Choose File  ${upload_file}   ${file_path}








