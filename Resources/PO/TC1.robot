*** Settings ***
Library    SeleniumLibrary
Resource  ../common.robot
Library     CSVLibrary
Library     String

*** Variables ***
${contactus_button}       xpath=//span[@data-alt='Contact us']
${name_field}                   //input[@id='cf-1']
${email_field}                  //input[@id='cf-2']
${mobile_field}                 //input[@id='cf-3']
${subject_field}                //input[@id='cf-4']
${message_field}                //textarea[@id='cf-5']
${file_path}               ${EXECDIR}/Resources/TestData/email_list.csv
${close_contactUS}         //a[@id='fancybox-close']
*** Keywords ***

Access Contact US
    Scroll Down     ${contactus_button}
    Wait Until Element is visible      ${contactus_button}
    Click Element    ${contactus_button}

Invalid Contact US
    [Arguments]    ${email}
    Access Contact US
    input text     ${name_field}          amira
    input text     ${email_field}         ${email}
    input text     ${mobile_field}        010023300000000
    input text     ${subject_field}       Test
    input text     ${message_field}       message test
    Submit Form     ${submit_btn}
    Assert on error message      ${error_message}
    Click element      ${close_contactUS}

Get From File
    ${text}   get file   ${file_path}
     @{list}   Split To Lines   ${text}
    log     ${list}
                 FOR    ${email}      IN     @{list}
                        Invalid Contact US     ${email}
                     END






