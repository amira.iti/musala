
*** Settings ***
Library    SeleniumLibrary
#Library  ExtendedSelenium2Library

*** Variables ***
${browser}     chrome

${url}                        https://www.musala.com/
${submit_btn}                 xpath=//input[@type='submit']
${error_message}              //*[@class="wpcf7-not-valid-tip"and contains(text(),'${email_error_message}')]
${location}                   xpath=//select[@id='get_location']
${open_postion}               xpath=//span[@data-alt='Check our open positions']
${skip_data_error}            //*[@class="wpcf7-not-valid-tip"and contains(text(),'${field_req_error_message}')]
${email_error_message}        The e-mail address entered is invalid
${field_req_error_message}    The field is required.


*** Keywords ***

Begin web test
   Open Browser   ${url}     ${browser}
    MAXIMIZE BROWSER WINDOW

End web test
    close Browser


Scroll Down
    [Arguments]   ${elment_id}
    Scroll Element Into View    ${elment_id}
    Wait Until Element is visible    ${elment_id}   timeout=5s
    Set Focus To Element    ${elment_id}


 Get Current URL
    ${current_url}   Get Location
    [Return]          ${current_url}

 Assert current url
     [Arguments]   ${current_url}   ${url}
     Should Be Equal As Strings      ${current_url}    ${url}

Assert on error message
   [Arguments]     ${error_message}
    Wait Until Element is visible     ${error_message}

Submit Form
     [Arguments]    ${submit_btn}
     Click Element   ${submit_btn}

Access Open Positions
    Wait Until Element is visible    ${open_postion}
    Click Element    ${open_postion}

Select Location
    [Arguments]     ${job_location}
    Wait Until Element is visible    ${location}
    Click Element    ${location}
    Select From List by Value    ${location}     ${job_location}



