# musala

# Why Robot Framework

- Open source
- Very easy to install
- RF is application and platform independent
- User doesn't need a programming language to write a Robot Framework test case & run it
- Supports Keyword driven, Data-driven and Behaviour-driven (BDD) approaches
- The use of Selenium2 library in RF will help  to automate the existing manual test cases of a project in a Rapid Phase
- Outstanding Reporting system: The RF automatically generates a report and html log after executing each build
- Robot Framework provides lots of libraries to test different application like Appium Library for Mobile Automation, Database Library for DB testing, Android Library etc.


## Key Feature List

- #### Test Case1 by using Template
- #### Test Case1 by using a csf file
- #### Test Case2 
- #### Test Case3
- #### Test Case4


## How to install Robot Framework with Python


- Robot Framework is implemented with Python, so you need to have Python installed.
On Windows machines, make sure to add Python to PATH during installation.

- Installing Robot Framework with pip is simple:

`pip install robotframework`

- To check that the installation was succesful, run

`robot --version`

- For a full guide, please see Installation instructions. It also covers topics such as running Robot Framework on Jython (JVM) and IronPython (.NET).

**Now you are ready to write your first tests!**

## Other Dependencies 

```
pip install robotframework
pip install robotframework-string
pip install robotframework-SeleniumLibrary
pip install robotframework OperatingSystem
pip install robotframework CSVLibrary
pip install robotframework string
pip install robotframework-pabot

```

- You will find all requirements in requirements.txt file

## To clone the project 

`https://gitlab.com/amira.iti/musala.git`


## Robot Framework Project structure 

- ###### Tests folder that contains all test cases 
- ###### Resources folder that will include all common and all page object pages 
- ###### Results Folder that all have the result report along with the tags

## Robot Framework Test Cases some examples 

- #### Test Case1

```
################## Running Commands ##################
# robot -d Results      -i     Happy        Tests
# robot -d Results      -i     Negative     Tests
# robot -d Results      -i     TC1     Tests
# robot -d Results      -i     file        Tests
# robot -d Results      -i     musala        Tests

*** Settings ***
Library       OperatingSystem
Resource      ../Resources/common.robot
Resource      ../Resources/PO/TC1.robot
#Library        DataDriver ../Resources/PO/TestData/email_list.csv
#Resource       ../Resources/PO/TestData/email_list.csv

Test Setup          Begin web test
Test Teardown       End web test

*** Variables ***

*** Test Cases ***

Contact US with filling invalid data using
     [Tags]     contactus    Happy  TC1     TCs    file     musala
     Get Data From File
     #Get From File
   
```
- #### PO for Test Case1

```
*** Settings ***
Resource  ../common.robot

*** Variables ***
${contactus_button}       xpath=//span[@data-alt='Contact us']
${name_field}                   //input[@id='cf-1']
${email_field}                  //input[@id='cf-2']
${mobile_field}                 //input[@id='cf-3']
${subject_field}                //input[@id='cf-4']
${message_field}                //textarea[@id='cf-5']
${file_path}               ${EXECDIR}/Resources/TestData/email_list.csv
${close_contactUS}         //a[@id='fancybox-close']
*** Keywords ***

Access Contact US
    Scroll Down     ${contactus_button}
    Wait Until Element is visible      ${contactus_button}
    Click Element    ${contactus_button}

Invalid Contact US
    [Arguments]    ${email}
    Access Contact US
    input text     ${name_field}          amira
    input text     ${email_field}         ${email}
    input text     ${mobile_field}        010023300000000
    input text     ${subject_field}       Test
    input text     ${message_field}       message test
    Submit Form     ${submit_btn}
    Assert on error message      ${error_message}
    Click element      ${close_contactUS}

Get Data From File
    ${text}   get file   ${file_path}
     @{list}   Split To Lines   ${text}
    log     ${list}
                 FOR    ${email}      IN     @{list}
                        Invalid Contact US     ${email}
                     END
```

- #### Test Case1 Using Template 
```
################## Running Commands ##################

# robot -d Results      -i     Happy        Tests
# robot -d Results      -i     Negative     Tests
# robot -d Results      -i     Template     Tests
# robot -d Results      -i     TC1          Tests
# robot -d Results      -i     musala        Tests

*** Settings ***
Resource      ../Resources/common.robot
Resource      ../Resources/PO/TC1.robot

Test Setup          Begin web test
Test Teardown       End web test

*** Variables ***

*** Test Cases ***

Contact US with filling invalid data using ${email}
     [Tags]     contactus    Happy  TC1     TCs     Template        musala
#     Access Contact US
     [Template]     Invalid Contact US
     aa
     $%%
     5##
     @@@
     aa@xx

```

## Robot Framework Running the tests 

- You can using the running commands in each test cases and below some of them 

### To run all test scenarios 
###### robot -d Results      -i     musala        Tests

#### To run all test scenarios for test case1
####### robot -d Results      -i     TC1        Tests

#### To run all test scenarios for test case2
####### robot -d Results      -i     TC2       Tests

#### To run all test scenarios for test case3
####### robot -d Results      -i     TC3        Tests

#### To run all test scenarios for test case4
####### robot -d Results      -i     TC4       Tests

#### To run all invalid scenarios test cases
####### robot -d Results      -i     invalid        Tests

## Robot Framework Running the tests 

- In Order to configure your browser in common file you can provide :

`${browser}     chrome`

Or

`${browser}     firefox`


## Robot Framework Running parallel test 

- I used pabot framework to run parallel test

`pip install -U robotframework-pabot`

#### To run all tests in parallel

`pabot --include musala Tests`


[For more details please check  ](https://mydeveloperplanet.com/2020/09/09/parallel-testing-with-robot-framework/)

