*** Settings ***
Library    SeleniumLibrary
Resource  ../common.robot

*** Variables ***
${career}                  xpath=//div[@id='menu']/ul[@class='nav']//a[@href='https://www.musala.com/careers/']
#${list_all_positions}      //*[@id="content"]/section/div[2]/article
${list_all_positions}           //*[@class="card-container"]/a

*** Keywords ***

Access Career Link
    Wait Until Element is visible    ${career}
    Click Element    ${career}

Get All Open position in a City
    ${matched elements}=    Get Webelements     ${list_all_positions}
    ${count} =  Get Element Count   ${list_all_positions}
    Log To Console  The total open positions are:${count} ${\n}
             FOR    ${element}      IN     @{matched elements}
                    ${position_url}=  get element attribute    ${element}     href
                    ${text}=    Get Text    ${element}
                    Log to console    Position Name:${text}
                    Log to console    More Info:${position_url} ${\n}
                 END

