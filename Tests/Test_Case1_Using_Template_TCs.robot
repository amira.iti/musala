################## Running Commands ##################

# robot -d Results      -i     Happy        Tests
# robot -d Results      -i     Negative     Tests
# robot -d Results      -i     Template     Tests
# robot -d Results      -i     TC1          Tests
# robot -d Results      -i     musala        Tests


*** Settings ***
Library       SeleniumLibrary
Resource      ../Resources/common.robot
Resource      ../Resources/PO/TC1.robot


Test Setup          Begin web test
Test Teardown       End web test

*** Variables ***

*** Test Cases ***

Contact US with filling invalid data using ${email}
     [Tags]     contactus    Happy  TC1     TCs     Template        musala
#     Access Contact US
     [Template]     Invalid Contact US
     aa
     $%%
     5##
     @@@
     aa@xx