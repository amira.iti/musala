################## Running Commands ##################

# robot -d Results      -i     Happy        Tests
# robot -d Results      -i     Negative     Tests
# robot -d Results      -i     TC2     Tests
# robot -d Results      -i     musala        Tests


*** Settings ***
Library           SeleniumLibrary
Resource         ../Resources/common.robot
Resource         ../Resources/PO/TC2.robot
Test Setup       Begin web test
Test Teardown    End web test

*** Variables ***
${company_url}            https://www.musala.com/company/
${fb_url}                 https://www.facebook.com/MusalaSoft?fref=ts

*** Test Cases ***

Access Company Profile Page
     [Tags]     contactus    Happy  TC2     TCs     musala
     Access Company Link
     ${current_url}     Get Current URL
     Assert current url      ${current_url}    ${company_url}
     scroll down    ${leadership}
     Access Facebook link
     ${current_url}     Get Current URL
     Assert current url      ${current_url}     ${fb_url}
     Assert Profile picture






