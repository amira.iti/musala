*** Settings ***
Library    SeleniumLibrary
Resource  ../common.robot

*** Variables ***
${company}             xpath=//div[@id='menu']/ul[@class='nav']//a[@href='https://www.musala.com/company/']
${company_url}         https://www.musala.com/company/
${leadership}          //h2[contains(.,'Leadership')]
${facebook_link}       //*[@class="musala musala-icon-facebook"]
${profile_picture}     //div[@class='_6tay']

*** Keywords ***

Access Company Link
    Wait Until Element is visible    ${company}
    Click Element    ${company}

Access Facebook link
    Click Element      ${facebook_link}
    Switch Window  title:Musala Soft - Home | Facebook


Assert Profile picture
    Wait Until Element is visible      ${profile_picture}














