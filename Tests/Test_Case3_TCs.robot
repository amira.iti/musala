################## Running Commands ##################

# robot -d Results      -i     Happy        Tests
# robot -d Results      -i     Negative     Tests
# robot -d Results      -i     TC3     Tests
# robot -d Results      -i     musala        Tests

*** Settings ***
Library           SeleniumLibrary
Resource         ../Resources/common.robot
Resource         ../Resources/PO/TC3.robot
Test Setup       Begin web test
Test Teardown    End web test

*** Variables ***
${open_position_url}           https://www.musala.com/careers/join-us/
${name_of_postion}             Experienced Automation QA Engineer


*** Test Cases ***

Apply for QA Job with invalid e-mail
     [Tags]     apply-qa    Happy  TC3     TCs      musala
     Access career Link
     Access Open Positions
     ${current_url}     Get Current URL
     Assert current url      ${current_url}    ${open_position_url}
     Select Location    Anywhere
     choose open position by name       ${name_of_postion}
     Assert Job Sections
     Scroll Down    ${apply_btn}
     Click To Apply
     Fill Applicant Data      amira soliman     aaaaa     010012333330000
     Submit Form        ${submit_btn}
     Upload CV
     Assert on error message    ${error_message}


Apply for QA Job with skip e-mail address
     [Tags]     apply-qa    Happy  TC3     TCs     musala
     Access career Link
     Access Open Positions
     ${current_url}     Get Current URL
     Assert current url      ${current_url}    ${open_position_url}
     Select Location    Anywhere
     choose open position by name       ${name_of_postion}
     Assert Job Sections
     Scroll Down    ${apply_btn}
     Click To Apply
     Fill Applicant Data      amira soliman     ${EMPTY}     010012333330000
     Submit Form        ${submit_btn}
     Upload CV
     Assert on error message    ${skip_data_error}










